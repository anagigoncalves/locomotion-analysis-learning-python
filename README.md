# locomotion-analysis-learning-python



## Package to do locomotion analysis on the treadmill in Python

Based on Darmohray et al (2019), this package contains a class with the relevant functions to do locomotion analysis on the treadmill (locomotion_analysis.py).

It also contains a simple main script that plots the learning curves for individual animals in a session (plot_learning_curves.py)
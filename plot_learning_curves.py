# -*- coding: utf-8 -*-
import os
import numpy as np
import matplotlib.pyplot as plt

#Path inputs - CHANGE
path_loco = 'C:\\Users\\Ana\\Documents\\test session\\'
print_plots = 1
Ntrials = 23
split_trials = 10
tied_trials = np.array([1, 2, 3])
pixel_to_mm = 1/1.98 #jovin setup
confidence_level_DLC = 0.9
exclude_bad_strides = 1

#import classes - CHANGE
os.chdir('C:\\Users\\Ana\\Documents\\PhD\\Dev\\miniscope_analysis\\')
import locomotion_class
loco = locomotion_class.loco_class(path_loco, pixel_to_mm)
path_save = path_loco+'grouped output\\'
    
animal_session_list = loco.animals_within_session()
animal_list = []
for a in range(len(animal_session_list)):
    animal_list.append(animal_session_list[a][0])
session_list = []
for a in range(len(animal_session_list)):
    session_list.append(animal_session_list[a][1])

#summary gait parameters
param_sym_name = ['coo', 'step_length', 'double_support', 'coo_stance', 'swing_length'] #CHANGE TO ADD OR DELETE GAIT PARAMETERS
param_sym = np.zeros((len(param_sym_name), len(animal_list), Ntrials))
for count_animal, animal in enumerate(animal_list):
    session = int(session_list[count_animal])
    filelist = loco.get_track_files(animal,session)
    for count_trial, f in enumerate(filelist):
        #Get paw excursions
        [final_tracks, tracks_tail, joints_wrist, joints_elbow, ear, bodycenter] = loco.read_h5(f, confidence_level_DLC)
        #Get swing and stance points
        [st_strides_mat, sw_pts_mat] = loco.get_sw_st_matrices(final_tracks, exclude_bad_strides)
        #Get forward paw excursions relative to the center of mass
        paws_rel = loco.get_paws_rel(final_tracks, 'X')
        #Compute gait parameters
        for count_p, param in enumerate(param_sym_name):
            param_mat = loco.compute_gait_param(bodycenter, final_tracks, paws_rel, st_strides_mat, sw_pts_mat, param)
            #Get front paw symmetry
            param_sym[count_p, count_animal, count_trial] = np.nanmean(param_mat[0])-np.nanmean(param_mat[2])

#%% Plot
#baseline subtracion of parameters
param_sym_bs = np.zeros(np.shape(param_sym))
for p in range(np.shape(param_sym)[0]):
    for a in range(np.shape(param_sym)[1]):
        bs_mean = np.nanmean(param_sym[p, a, :tied_trials[-1]-1])
        param_sym_bs[p, a, :] = param_sym[p, a, :] - bs_mean #will give an error if animals did sessions of different sizes

#plot symmetry baseline subtracted
for p in range(np.shape(param_sym)[0]):
    fig, ax = plt.subplots(figsize=(5, 10), tight_layout=True)
    rectangle = plt.Rectangle((tied_trials[-1]+0.5, min(param_sym_bs[p, :, :].flatten())), split_trials,
            max(param_sym_bs[p, :, :].flatten())-min(param_sym_bs[p, :, :].flatten()), fc='lightgrey',alpha=0.45)
    plt.gca().add_patch(rectangle)
    plt.hlines(0, 1, len(param_sym_bs[p, a, :]), colors='grey', linestyles='--')
    for a in range(np.shape(param_sym)[1]):
        plt.plot(np.linspace(1,len(param_sym_bs[p, a, :]), len(param_sym_bs[p, a, :])),param_sym_bs[p, a, :], label=animal_list[a], linewidth=2)
    ax.set_xlabel('Trial', fontsize=20)
    ax.legend(frameon=False)
    ax.set_ylabel(param_sym_name[p].replace('_', ' '), fontsize=20)
    if p == 2:
        plt.gca().invert_yaxis()
    plt.xticks(fontsize=16)
    plt.yticks(fontsize=16)
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    if print_plots:
        if not os.path.exists(path_save):
            os.mkdir(path_save)
        plt.savefig(path_save+param_sym_name[p]+'_sym_bs', dpi=128)